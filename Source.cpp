#include <windows.h>
#include <stdio.h>
#include <iostream>
#include "Helper.h"
#include <vector>
#include <tchar.h>
#include <strsafe.h>

#define BUFSIZE MAX_PATH

void secret_command()
{
	typedef int(__stdcall *f_funci)();
	HINSTANCE hGetProcIDDLL = LoadLibrary("C:\\MagshimimYearB\\cpp\\Lesson18\\Examples\\SECRET.dll");

	f_funci funci = (f_funci)GetProcAddress(hGetProcIDDLL, "TheAnswerToLifeTheUniverseAndEverything");

	std::cout << funci() << std::endl;
}

void ls_command()
{
	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;
	TCHAR Buffer[BUFSIZE];
	DWORD dwRet;
	dwRet = GetCurrentDirectory(BUFSIZE, Buffer);
	// Check that the input path plus 3 is not longer than MAX_PATH.
	// Three characters are for the "\*" plus NULL appended below.

	StringCchLength(Buffer, MAX_PATH, &length_of_arg);


	StringCchCopy(szDir, MAX_PATH, Buffer);
	StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.

	hFind = FindFirstFile(szDir, &ffd);


	// List all the files in the directory with some info about them.

	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
		}
		else
		{
			filesize.LowPart = ffd.nFileSizeLow;
			filesize.HighPart = ffd.nFileSizeHigh;
			_tprintf(TEXT("  %s   %ld bytes\n"), ffd.cFileName, filesize.QuadPart);
		}
	} while (FindNextFile(hFind, &ffd) != 0);


	FindClose(hFind);
}



int main()
{

	TCHAR Buffer[BUFSIZE];
	DWORD dwRet;
	std::string cmd;
	std::vector<std::string> words;
	typedef HANDLE HWND;
	while (true)
	{
		std::cout << ">> ";
		std::cin >> cmd;
		if (cmd == "cd")
		{
			std::cin >> Buffer;
			if (!SetCurrentDirectory(Buffer))
			{
				printf("SetCurrentDirectory failed (%d)\n", GetLastError());
				return 0;
			}
		}
		else if (cmd == "create")
		{
			std::cin >> Buffer;
			if (!CreateDirectory(Buffer, NULL))
			{
				printf("CreateDirectory failed (%d)\n", GetLastError());
				return 0;
			}	
		}
		else if (cmd == "pwd")	
		{
			dwRet = GetCurrentDirectory(BUFSIZE, Buffer);
			if (dwRet == 0)
			{
				printf("GetCurrentDirectory failed (%d)\n", GetLastError());
				return 0;
			}
			if (dwRet > BUFSIZE)
			{
				printf("Buffer too small; need %d characters\n", dwRet);
				return 0;
			}
			_tprintf(TEXT("%s \n"), Buffer);
		}
		else if (cmd == "ls")
		{
			ls_command();
		}
		else if (cmd == "secret")
		{
			secret_command();
		}
		else
		{
			DWORD dwMillisec = 1000;
			HANDLE handle = INVALID_HANDLE_VALUE;
			if (WinExec(cmd.c_str(), 0))
			{
				//do something
			}
		}
		
	}


	return 0;
}